local sensorInfo = {
	name = "GetMapInfo",
	desc = "Initializes heightMap, returns the heightMap + platform positions (northwesternmost and southeasternmost point)",
	author = "Soipask",
	date = "2021-09-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local SpringGetGroundHeight = Spring.GetGroundHeight

local platformCount = 0
local platforms = {}
local width = Game.mapSizeZ
local length = Game.mapSizeX

local function IsNewPlatform(x, z, chunksize)
	for i=1,platformCount  do
		if platforms[i][1].x - chunksize <= x and x <= platforms[i][2].x + chunksize and platforms[i][1].z - chunksize <= z and z <= platforms[i][2].z + chunksize then
			return false
		end
	end
	
	return true
end

local function FindSouthWestPlatformPoint(x, z, chunksize, groundHeight)
	southeastpoint = {}
	bool = true
	
	for i=x,length,chunksize do
		if SpringGetGroundHeight(i,z) <= groundHeight then
			southeastpoint.x = i - chunksize
			bool = false
			break
		end
	end
	if bool then southeastpoint.x = length end
	
	bool = true
	for j=z,width,chunksize do
		if SpringGetGroundHeight(x,j) <= groundHeight then
			southeastpoint.z = j - chunksize
			bool = false
			break
		end
	end
	if bool then southeastpoint.z = width end
	
	return southeastpoint
end

-- @description return shallow copy of given table
-- @parameter chunksize [number] - determines how dense the heightMap will be
return function(chunksize)
	local groundHeight = 130
	
	local heightMap = {}
	
	for x=chunksize,length,chunksize do
		heightMap[x / chunksize] = {}
		for z=chunksize,width,chunksize do
			heightMap[x / chunksize][z / chunksize] = SpringGetGroundHeight(x,z)
		end
	end
	
	for x=chunksize,length,chunksize do
		for z=chunksize,width,chunksize do
			if heightMap[x / chunksize][z / chunksize] > groundHeight and IsNewPlatform(x, z, chunksize) then 
				platformCount = platformCount + 1
				platforms[platformCount] = {}
				platforms[platformCount][1] = {x = x, z = z}
				platforms[platformCount][2] = FindSouthWestPlatformPoint(x, z, chunksize, groundHeight)
			end
		end
	end 
	
	return {
		width = width,
		length = length,
		heightMap = heightMap,
		platforms = platforms,
		groundHeight = groundHeight,
		platformCount = platformCount
	}
end