local sensorInfo = {
	name = "WindDirection",
	desc = "Return strength and normals of the actual wind.",
	author = "Soipask",
	date = "2021-09-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind direction normalized (only direction)
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
	
	return {
		normDirX = normDirX,
		normDirY = normDirY,
		normDirZ = normDirZ,
	}
end