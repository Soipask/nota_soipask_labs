local sensorInfo = {
	name = "IsEnemyInBetween",
	desc = "Checks if enemy is between given positions",
	author = "Soipask",
	date = "2021-09-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return true, if enemy is between given positions
return function(positionA, positionB, enemyPositions)
	for i=1,#enemyPositions  do
		x = enemyPositions[i].x
		z = enemyPositions[i].z
		if positionA.x <= x and x <= positionB.x and positionA.z <= z and z <= positionB.z then
			return true
		end
	end
	
	return false
end