local sensorInfo = {
	name = "PickAny",
	desc = "Picks first element in a table",
	author = "Soipask",
	date = "2021-09-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return shallow copy of given table
return function(exploringUnits)
	picked = table.remove(exploringUnits)
	
	return {
		picked = picked,
		units = exploringUnits
	}
end