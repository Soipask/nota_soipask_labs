local sensorInfo = {
	name = "windDebugDirection",
	desc = "Shows direction of the wind using debug drawing.",
	author = "Soipask",
	date = "2021-09-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind debug drawing from one of the battle commanders
return function()
	local norm = Sensors.nota_soipask_labs.Wind()
	
	-- debugging, writing line from one of the battle commanders' position in direction of the wind
	local unitID = units[1]
	local x,y,z = Spring.GetUnitPosition(unitID)
	if (Script.LuaUI('exampleDebug_update')) then
		Script.LuaUI.exampleDebug_update(
			1, -- key
			{	-- data
				startPos = Vec3(x,y,z), 
				endPos = Vec3(x,y,z) + Vec3(norm.normDirX * 500, norm.normDirY * 500, norm.normDirZ * 500)
			}
		)
	end
	
	return true
end