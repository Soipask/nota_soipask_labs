local sensorInfo = {
	name = "GetUnitPositionVector",
	desc = "Returns vector of the given unit position.",
	author = "Soipask",
	date = "2021-09-17",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return vector of the given unit position
return function(unitID)
	local x,y,z = SpringGetUnitPosition(unitID)
	
	return Vec3(x,y,z)
end