local sensorInfo = {
	name = "GetAllLoadedTransporters",
	desc = "Returns all transporters that anything loaded.",
	author = "Soipask",
	date = "2021-09-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

-- @description return vector of the given unit position
return function(atlases)
	local loaded = {}
	
	for unitID, state in pairs(atlases) do
		local transportees = SpringGetUnitIsTransporting(unitID)
		if transportees ~= nil and #transportees > 0 then
			loaded[#loaded+1] = unitID
		end
	end
	
	return loaded
end