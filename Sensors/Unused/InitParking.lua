local sensorInfo = {
	name = "InitParking",
	desc = "Initializes parking spots based on safe area.",
	author = "Soipask",
	date = "2021-09-19",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return vector of the given unit position
return function(safeArea) -- safeArea = {center <- Vec3, radius <- number}
	local parking = {spots = {}}
	
	-- safe area is circle, we'll make a square in the middle
	local squareRadius = math.sqrt((safeArea.radius^2)/2)
	parking.first = Vec3(safeArea.center.x - squareRadius,safeArea.center.y, safeArea.center.z - squareRadius)
	parking.difference = squareRadius / 5
	
	for i=1,10 do
		parking.spots[i] = {}
		for j=1,10 do
			parking.spots[i][j] = "free"
		end
	end
	
	return parking
end