local sensorInfo = {
	name = "UpdateParkingSpots",
	desc = "Updates parking spots.",
	author = "Soipask",
	date = "2021-09-19",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SpringGetMyTeamID = Spring.GetMyTeamID

-- @description return vector of the given unit position
return function(parking)
	local firstPos = Vec3(parking.first.x - parking.difference / 2, parking.first.y, parking.first.z - parking.difference / 2)
	local difference = parking.difference
	
	for i=1,10 do
		for j=1,10 do
			rectangle = {firstPos.x + (i-1)*difference, firstPos.z + (i-1)*difference, firstPos.x + i*difference, firstPos.z + i*difference}
			units = SpringGetUnitsInRectangle(rectangle[1],rectangle[2],rectangle[3],rectangle[4],SpringGetMyTeamID())
			Spring.Echo(#units)
			Spring.Echo(units)
			
			if #units > 0 then
				parking.spots[i][j] = "occupied"
			end
		end
	end
	
	return parking
end