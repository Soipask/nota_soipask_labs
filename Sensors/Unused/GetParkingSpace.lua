local sensorInfo = {
	name = "GetParkingSpace",
	desc = "Returns precise parking position for transporter to unload units.",
	author = "Soipask",
	date = "2021-09-19",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return vector of the given unit position
return function(parking)
	local x
	local z
	local parkingSpot = Vec3(0,0,0)
	
	for i=1,10 do
		for j=1,10 do
			if parking.spots[i][j] == "free" then 
				x = i
				z = j
				parking.spots[i][j] = "occupied"
			end
		end
	end
	
	if x ~= nil then 
		parkingSpot = Vec3(parking.first.x + x*parking.difference,parking.first.y,parking.first.z + z*parking.difference)
	end
	
	return parkingSpot
end