local sensorInfo = {
	name = "IsTransporterTransportingThis",
	desc = "Returns true if given transporter is transporting given unit.",
	author = "Soipask",
	date = "2021-09-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringValidUnitID = Spring.ValidUnitID
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

local safeUnits = {}

-- @description return vector of the given unit position
return function(transporter, toBeRescued)
	
	if not SpringValidUnitID(toBeRescued) then return false end
	
	local transportees = SpringGetUnitIsTransporting(transporter)
	if transportees ~= nil and transportees[1] == toBeRescued then
		return true
	else
		return false
	end
end