local sensorInfo = {
	name = "UpdateReservationSystem",
	desc = "Updates reservation system - deletes dead transporters, re-endangers units sliding out of safe area.",
	author = "Soipask",
	date = "2021-09-18",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SpringValidUnitID = Spring.ValidUnitID

local safeUnits = {}

local function IsReallySafe(unitID)
	local unitDefID = SpringGetUnitDefID(unitID)
	
	-- if it is a building, then it is safe (cannot be transported)
	if UnitDefs[unitDefID].cantBeTransported then 
		return true 
	end
	
	-- if it is in a safeArea
	for index, safeUnitID in ipairs(safeUnits) do
		if safeUnitID == unitID then
			return true
		end
	end
	
	return false
end

-- @description return vector of the given unit position
return function(reservationSystem, allUnits)
	
	for unitID, state in pairs(reservationSystem.atlases) do
		-- check validity of the unit
		if not SpringValidUnitID(unitID) then
			reservationSystem.atlases[unitID] = "dead"
		end
	end
	
	-- rectangle around safe area - do not need to save thos units, because they already are
	local safeArea = reservationSystem.safeArea
	local safetyRectangle = {
		safeArea.center.x - safeArea.radius, 
		safeArea.center.z - safeArea.radius,
		safeArea.center.x + safeArea.radius,
		safeArea.center.z + safeArea.radius}
		
	safeUnits = SpringGetUnitsInRectangle(safetyRectangle[1],safetyRectangle[2],safetyRectangle[3],safetyRectangle[4],Spring.GetMyTeamID())
	
	-- check all units
	for i=1,#allUnits do
		unitID = allUnits[i]
		
		-- check validity of unitID
		if not SpringValidUnitID(unitID) then
			reservationSystem.toBeRescued[unitID] = "dead"
		elseif not IsReallySafe(unitID) and reservationSystem.toBeRescued[unitID] ~= "picked"  then
			-- if not picked and not safe -> endangered
			reservationSystem.toBeRescued[unitID] = "endangered"
		end
	end
	
	return reservationSystem
end