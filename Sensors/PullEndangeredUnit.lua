local sensorInfo = {
	name = "PullEndangeredUnit",
	desc = "Gets some endangered unit from reservation system.",
	author = "Soipask",
	date = "2021-09-18",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return vector of the given unit position
return function(reservationSystem)
	local selectedID
	
	for unitID, state in pairs(reservationSystem.toBeRescued) do
		if state == "endangered" then
			selectedID = unitID
			reservationSystem.toBeRescued[unitID] = "picked"
			break
		end
	end
	
	return selectedID
end