local sensorInfo = {
	name = "ShallowCopy",
	desc = "Performs shallow copy of given array/table",
	author = "Soipask",
	date = "2021-09-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return shallow copy of given table
return function(units)
  local copy = {}
  for k,v in pairs(units) do
    copy[k] = v
  end
  return copy
end