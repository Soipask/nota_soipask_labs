local sensorInfo = {
	name = "PullFreeTransporter",
	desc = "Gets free transporter from reservation system (ideally some with max health).",
	author = "Soipask",
	date = "2021-09-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringValidUnitID = Spring.ValidUnitID


local function FindMaxHealthUnits(maxUnits, reservationSystem)
	local pickedUnits = {}
	for unitID, state in pairs(reservationSystem.atlases) do
		local health, maxHealth = SpringGetUnitHealth(unitID)
		if state == "free" and health ~= nil and health == maxHealth then
			pickedUnits[#pickedUnits + 1] = {unitID = unitID, healthPct = health / maxHealth}
			reservationSystem.atlases[unitID] = "picked"
		end
		
		if #pickedUnits == maxUnits then break end
	end
	
	return pickedUnits
end

local function FindMostHealthyUnits(maxUnits, reservationSystem)
	local pickedUnits = {}
	for i=1,maxUnits do	
		local pickedUnit = {}
		
		local selectedID
		local selectedUnitHealth = 0
		local selectedUnitMaxHP = 0
		for unitID, state in pairs(reservationSystem.atlases) do
			local health, maxHealth = SpringGetUnitHealth(unitID)
			if state == "free" and health ~= nil and health > selectedUnitHealth then
				selectedID = unitID
				selectedUnitHealth = health
				selectedUnitMaxHP = maxHealth
			end
		end
		
		if selectedID ~= nil then
			pickedUnits[#pickedUnits + 1] = {unitID = selectedID, healthPct = selectedUnitHealth / selectedUnitMaxHP}
			reservationSystem.atlases[selectedID] = "picked"
		end
	end
	
	return pickedUnits
end

local function PickBest(array)
	-- check validity
	-- pick the one with best health
	local selectedID
	local selectedUnitHealth = 0
	for i=1,#array do
		if SpringValidUnitID(array[i].unitID) then
			if array[i].healthPct == 1 then return array[i].unitID end
			
			if array[i].healthPct > selectedUnitHealth then
				selectedID = unitID
				selectedUnitHealth = array[i].healthPct
			end
		end
	end
	
	return selectedID
end

-- @description return vector of the given unit position
return function(reservationSystem)
	local maxUnits = 3
	local pickedUnits = {}
	
	-- find max health units
	pickedUnits = FindMaxHealthUnits(maxUnits, reservationSystem)
	
	local selectedID = PickBest(pickedUnits)
	
	-- if haven't found any good with maxHealth, find most healthy units
	if selectedID == nil then
		pickedUnits = FindMostHealthyUnits(maxUnits, reservationSystem)
		selectedID = PickBest(pickedUnits)
	end
	
	-- free the picked ones except the selected one
	if selectedID ~= nil then 
		for i=1,#pickedUnits do
			reservationSystem.atlases[pickedUnits[i].unitID] = "free"
		end
		
		reservationSystem.atlases[selectedID] = "occupied" 
	end
	
	return selectedID
end