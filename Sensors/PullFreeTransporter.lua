local sensorInfo = {
	name = "PullFreeTransporter",
	desc = "Gets free transporter from reservation system (ideally some with max health).",
	author = "Soipask",
	date = "2021-09-18",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitHealth = Spring.GetUnitHealth

-- @description return vector of the given unit position
return function(reservationSystem)
	local selectedID
	local selectedUnitHealth = 0
	
	for unitID, state in pairs(reservationSystem.atlases) do
		local health, maxHealth = SpringGetUnitHealth(unitID)
		if state == "free" and health ~= nil and selectedUnitHealth < health then
			selectedID = unitID
			selectedUnitHealth = health
			if health == maxHealth then break end
		end
	end
	
	if selectedID ~= nil then reservationSystem.atlases[selectedID] = "occupied" end
	
	return selectedID
end