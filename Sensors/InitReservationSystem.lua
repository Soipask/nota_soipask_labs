local sensorInfo = {
	name = "InitReservationSystem",
	desc = "Initializes reservation system for atlases, endangered units and their unloading spots.",
	author = "Soipask",
	date = "2021-09-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle

local safeUnits = {}

local function IsWorthSaving(unitID)
	local unitDefID = SpringGetUnitDefID(unitID)
	
	-- if it is a building, then it is not worth saving as it cannot be transported
	if UnitDefs[unitDefID].cantBeTransported then 
		return false 
	end
	
	-- if it is in a safeArea
	for index, safeUnitID in ipairs(safeUnits) do
		if safeUnitID == unitID then
			return false
		end
	end
	
	return true
end

-- @description return vector of the given unit position
return function(atlases, unitsToBeRescued, safeArea)
	local reservationSystem = {
		atlases = {},
		toBeRescued = {},
		safeArea = safeArea
	}
	
	for i=1,#atlases do
		reservationSystem.atlases[atlases[i]] = "free"
	end
	
	-- rectangle around safe area - do not need to save thos units, because they already are
	local safetyRectangle = {
		safeArea.center.x - safeArea.radius, 
		safeArea.center.z - safeArea.radius,
		safeArea.center.x + safeArea.radius,
		safeArea.center.z + safeArea.radius}
		
	safeUnits = SpringGetUnitsInRectangle(safetyRectangle[1],safetyRectangle[2],safetyRectangle[3],safetyRectangle[4],Spring.GetMyTeamID())
	
	for i=1,#unitsToBeRescued do
		unitID = unitsToBeRescued[i]
		if IsWorthSaving(unitID) then
			reservationSystem.toBeRescued[unitID] = "endangered"
		end
	end
	
	return reservationSystem
end