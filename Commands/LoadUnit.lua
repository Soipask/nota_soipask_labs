function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Transport given unit using transporter",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "toBeRescued",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringValidUnitID = Spring.ValidUnitID
local SpringGetUnitDefID = Spring.GetUnitDefID

local function SuccessConditionsSucceeded(transporter, toBeRescued)
	-- the unit to be rescued is loaded into a correct transporter
	if SpringGetUnitTransporter(toBeRescued) == transporter then
		return true
	end
	
	return false
end

local function FailConditionsFailed(transporter, toBeRescued)
	-- the transporter is dead
	if not SpringValidUnitID(transporter) then
		Logger.warn("Invalid transporter UnitId")
		return true
	end
	
	-- the unit to be rescued is dead
	if not SpringValidUnitID(toBeRescued) then
		Logger.warn("Invalid toBeRescued UnitId")
		return true
	end
	
	
	-- the unit to be rescued is not loaded into a correct transporter
	local transportingUnit = SpringGetUnitTransporter(toBeRescued)
	if transportingUnit ~= nil and transportingUnit ~= transporter then
		Logger.warn("toBeRescued transported by another unit")
		return true
	end
	
	return false
end

local function FirstTimeFailureConditionsFailed(transporter, toBeRescued)
	local transporterDefID = SpringGetUnitDefID(transporter)
	local toBeRescuedDefID = SpringGetUnitDefID(toBeRescued)
	
	-- the unit to be rescued cannot be transported - first time check
	if UnitDefs[toBeRescuedDefID].cantBeTransported then
		Logger.warn("toBeRescued cant be transported")
		return true 
	end
	
	-- the transporter cannot transport/load -- first time check
	if not UnitDefs[transporterDefID].isTransport then 
		Logger.warn("transporter is not transport")
		return true 
	end
	
	return false
end

function Run(self, units, parameter)
	local transporter = parameter.transporter -- UnitId
	local toBeRescued = parameter.toBeRescued -- UnitId
	
	-- check success, fail ...
	if SuccessConditionsSucceeded(transporter, toBeRescued) then return SUCCESS end
	
	if FailConditionsFailed(transporter, toBeRescued) then return FAILURE end
	
	-- first run
	if not self.initialized then
		-- at first, check first-time checks
		if FirstTimeFailureConditionsFailed(transporter, toBeRescued) then return FAILURE end
		
		-- give command
		SpringGiveOrderToUnit(transporter, CMD.LOAD_UNITS, {toBeRescued}, {})
		self.initialized = true
	end
	
	return RUNNING
end


function Reset(self)
	self.initialized = false
end
