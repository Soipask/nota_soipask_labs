function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Transport given unit using transporter",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "safeArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringValidUnitID = Spring.ValidUnitID
local SpringGetUnitDefID = Spring.GetUnitDefID

local function SuccessConditionsSucceeded(transporter)
	local transportees = SpringGetUnitIsTransporting(transporter)
	if transportees == nil or #transportees == 0 then
		return true
	end
	
	return false
end

local function FailConditionsFailed(transporter)
	-- the transporter is dead
	if not SpringValidUnitID(transporter) then
		Logger.warn("Invalid transporter UnitId")
		return true
	end
	
	return false
end

local function FirstTimeFailureConditionsFailed(transporter)
	local transporterDefID = SpringGetUnitDefID(transporter)
	
	-- the transporter cannot transport/load -- first time check
	if UnitDefs[transporterDefID].isTransport == false then 
		Logger.warn("transporter is not transport")
		return true 
	end
	
end

function Run(self, units, parameter)
	local transporter = parameter.transporter -- UnitId
	local safeArea = parameter.safeArea -- array of num
	
	-- check success, fail ...
	if SuccessConditionsSucceeded(transporter) then return SUCCESS end
	
	if FailConditionsFailed(transporter) then return FAILURE end
	
	-- first run
	if not self.initialized then
		-- at first, check first-time checks
		if FirstTimeFailureConditionsFailed(transporter) then return FAILURE end
		
		-- give command
		SpringGiveOrderToUnit(transporter, CMD.UNLOAD_UNITS, {safeArea[1],safeArea[2],safeArea[3],safeArea[4]}, {})
		self.initialized = true
	end
	
	return RUNNING
end


function Reset(self)
	self.initialized = false
end
