nota_soipask_labs
====

- [dependencies](./dependencies.json)

Trees
----

* sandsail
* ctp2
* moveTwice (ctp2)

Sensors
----

* Wind (sandsail)
* DebugWindDirection (sandsail)
* ShallowCopy (ctp2)
* GetMapInfo (ctp2)
* PickAny (ctp2)
* IsEnemyInBetween (ctp2)
* InitReservationSystem (ttdr)
* PullFreeTransporter (ttdr)
* PullEndangeredUnit (ttdr)
* UpdateReservationSystem (ttdr)

Commands
----

* LoadUnit (ttdr)
* UnloadUnit (ttdr)